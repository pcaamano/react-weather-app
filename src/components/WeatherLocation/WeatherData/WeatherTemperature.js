import React from 'react';
import WeatherIcons from 'react-weathericons';
import {
  CLOUD,
  CLOUDY,
  SUN,
  RAIN,
  SNOW,
  WINDY
} from '../../../constants/weathers';
import PropTypes from 'prop-types';
import './styles.css';

const icons = {
  [CLOUD]: "cloud",
  [CLOUDY]: "cloudy",
  [SUN]: "day-sunny",
  [RAIN]: "rain",
  [SNOW]: "snow",
  [WINDY]: "windy"
}

const getWeatherIcon = weatherState => {
  const icon = icons[weatherState];
  //const sizeIcon = '4x';
  if(icon)
    return <WeatherIcons name={icon} size="2x" className="wicon" />;
  else
    return <WeatherIcons name={"day-sunny"} size="2x" className="wicon" />;
}

const WeatherTemperature = ({ temperature, weatherState}) => (
  <div className='weatherTemperature'>
    {
      // se requiere {} para poder agregar text JS dentro de un JSX
      getWeatherIcon(weatherState)
    }
    <span className="temperature">{`${temperature}`}</span>
    <span className="temperatureType">{` C°`}</span>
  </div>
);

/** VALIDACION
 * se define el tipo de parametro que se espera en WeatherTemperature 
 * y se define como requerido (no puede faltar) c/ parametro
 **/
WeatherTemperature.propTypes = {
  temperature: PropTypes.number.isRequired,
  weatherState: PropTypes.string.isRequired
};

export default WeatherTemperature;