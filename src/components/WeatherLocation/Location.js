import React from 'react';
import PropTypes from 'prop-types';
import './styles.css';

const Location = ( { data: {city} }) => {
  return (
    <div className="locationCont">
      <h1>{ city }</h1>
    </div>
  );
}

/** definicion de city dentro de data y el tipo de dato **/
Location.propTypes = {
  // ptsr + TAB
  data: PropTypes.shape({
    city: PropTypes.string.isRequired,
  })
};

export default Location;

/** Props param
 * forma clasica de extraer una propiedad de los parametros recibidos 
**/
//const Location = (props) => {
  //const city = props.city;

 /** Destructuring: 
  * evita tener que aclarar el subnivel dentro de las props
  * const { city } = props;
**/