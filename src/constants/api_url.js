/** OPEN WEATHER MAP API CONNECTION **/
const location = "Buenos Aires,ar";
const api_key = "5aeea37d779a08bc532e7a4be1c7af3b";
const url_base_weather = "http://api.openweathermap.org/data/2.5/weather";
const api_weather = `${url_base_weather}?q=${location}&appid=${api_key}`;

export default api_weather;