import convert from 'convert-units';
import {
  SUN
} from "../constants/weathers";

/** Extraigo datos necesarios de la respuesta del API **/
const weatherData = weatherData => {
  const { humidity, temp } = weatherData.main;
  const { name } = weatherData;
  const { speed } = weatherData.wind;
  const weatherState = SUN;
  const temperature = getTempInCelcius(temp);
  const data = {
    humidity,
    temperature,
    city: name,
    wind: speed,
    weatherState
  }
  return data;
}

/** conversion de temperatura en kelvin a celcius */
const getTempInCelcius = temp => {
  return Number(
    convert(temp)
      .from("K")
      .to("C")
      .toFixed(2)
  );
}

export default weatherData;