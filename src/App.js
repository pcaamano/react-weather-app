import React from 'react';

/** COMPONENTS **/
import WeatherLocation from './components/WeatherLocation';

function App() {
  return (
    <div className="App">
      <WeatherLocation></WeatherLocation>
    </div>
  );
}

export default App;
